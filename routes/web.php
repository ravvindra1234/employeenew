<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/employee', function () {
  //  return view('welcome');
//});
//Route::get('employee','WebController@index')->name('employee');

Route::get('/employee', 'WebController@index');
//Route::get('/empadd', 'WebController@empadd');
Route::get('/employee/add','WebController@empadd');
Route::post('employee/store', 'WebController@store')->name('employee.store');

Route::get('/employee/edit/{id}','WebController@edit');
Route::get('/employee/deleteemp/{id}', 'WebController@deleteemp');
Route::post('/employee/update', 'WebController@update')->name('employee.update');

