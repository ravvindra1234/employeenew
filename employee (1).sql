-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2021 at 06:21 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myeventz`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `profileimage` varchar(200) NOT NULL,
  `brthdate` varchar(200) NOT NULL,
  `currentaddress` varchar(200) NOT NULL,
  `permenantaddress` varchar(200) NOT NULL,
  `role` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `firstname`, `lastname`, `email`, `profileimage`, `brthdate`, `currentaddress`, `permenantaddress`, `role`, `status`) VALUES
(1, 'asasasa111111', 'saa', 'assaasas@xxx.com', 'sassa', '2021-07-07', 'aassa', 'ssaasa', '1', 1),
(2, 'jdkskjjkd111', 'djkdkjjks', 'sdsdd@ll.com', 'ddssssssss', '2021-07-14', 'dsssdsd', 'dddddss', '1', 1),
(3, 'ddf', 'fddf@sss.com', 'fddffdf@ddd.com', 'fdfd@sd.com', 'fdfdf', 'fff', 'dd', '1', 0),
(4, 'ddf', 'fddf@sss.com', 'fddffdf@ddd.com', 'fdfd@sd.com', 'fdfdf', 'fff', 'dd', '1', 1),
(5, 'ddf', 'fddf@sss.com', 'fddffdf@ddd.com', 'fdfd@sd.com', 'fdfdf', 'fff', 'dd', '1', 1),
(6, 'ss', 'sssssss', 'fddffdf@ddd.com', '', 'fdfdf', 'ddd', 'ddd', '1', 1),
(7, 'assa', 'ssss', 'ghg@hgh.com', '198063663420210711114007.jpg', 'ass', 'sasa', 'sas', '1', 1),
(8, 'ssdsd', 'dds', 'sdsd@kkk.com', '84680533720210711122306.jpg', '2021-07-16', 'dssd', 'dssd', '1', 1),
(9, 'ass', 'sas', 'ss@jj.com', '61628826120210711151217.jpg', '2021-07-11', 'ddds', 'dssd', 'tester,srdeveloper', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
