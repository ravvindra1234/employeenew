<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Jobs\SendMail;
use Mail;
use Session;
use Redirect;
use Config;
use App\Jobs\UserSystemInfoHelper as USIF;

class WebController extends Controller
{

    public function index()
    {
    	$data['employee']			=  DB::table('employee')->whereStatus(1)->get();
    	return view("index",$data);
	}
	
	public function empadd(){
	return view("addemp");
	}
	
	
	public function edit($id){
	$data['employee'] = \DB::table('employee')
			->select('*')
			->where("employee_id", '=', $id)
			->get();
	  	return view("editemp",$data);
	}
	
	
	public function update(Request $request){
		
			$logoschool  = $file_name ="";
			$role=implode(',',$request['role']);
		
			//print_r($request->file('profileimage'));
			//die('123');
			
		if ($request->hasFile('profileimage')) 
		{
			global $logoschool;
        $destinationPath = 'public/images/';
        $files = $request->file('profileimage'); 

    
       // $logoschool  =  $file_name = $files->getClientOriginalName(); 
		$logoschool  =  $file_name = rand()."".date('YmdHis').".".$files->getClientOriginalExtension();
      $files->move($destinationPath , $file_name);
	  
	   // move files to destination folder
    
        }
	
		
		if($logoschool){
		 \DB::table('employee')
         ->where('employee_id',$request->id)
         ->update(['firstname' => $request['fname'], 'lastname' => $request['lname'], 'email' => $request['email'], 'profileimage' => $logoschool, 'brthdate' => $request['birthdate'], 'currentaddress' => $request['caddress'], 'permenantaddress' => $request['paddress'], 'role' => $role]);
		}else{
		 \DB::table('employee')
         ->where('employee_id',$request->id)
         ->update(['firstname' => $request['fname'], 'lastname' => $request['lname'], 'email' => $request['email'], 'brthdate' => $request['birthdate'], 'currentaddress' => $request['caddress'], 'permenantaddress' => $request['paddress'], 'role' => $role]);
			
		}		
		
		
		return redirect('employee')->with('success', 'Employee updated successfully.');
		
		
		
		
	}
	
	public function store(Request $request) {	
		$role=implode(',',$request['role']);
		$logoschool  = $file_name ="";
		if ($request->hasFile('profileimage')) 
		{
			global $logoschool;
        $destinationPath = 'public/images/';
        $files = $request->file('profileimage'); 
		$logoschool  =  $file_name = rand()."".date('YmdHis').".".$files->getClientOriginalExtension();
      $files->move($destinationPath , $file_name);
	  
        }
				
\DB::table('employee')->insert(
			['firstname' => $request['fname'], 'lastname' => $request['lname'], 'email' => $request['email'], 'profileimage' => $logoschool, 'brthdate' => $request['birthdate'], 'currentaddress' => $request['caddress'], 'permenantaddress' => $request['paddress'], 'role' => htmlentities($role)]);
			return redirect('employee')->with('success', 'Employee created successfully.');
	}
	
	public function deleteemp($id){
		
	\DB::table('employee')
			->where("employee_id", '=', $id)
			->update(['status' => '0']);
	return redirect('employee')->with('success', 'Employee deleted successfully.');
	}
	
	
}
?>