@extends('layout')

@section('content')

<div class="mt-5">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  <div>
      <a class="btn btn-info  pull-right" href="{{ url('employee/add') }}"><i class="fa fa-plus"></i> Add New</a>
     </div>
  <table class="table">
    <thead>
        <tr class="table-primary">
          <td># ID</td>
          <td>First Name</td>
          <td>Last Name</td>
          <td>Email</td>
          <td>Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($employee as $employee)
        <tr>
            <td>{{$employee->employee_id}}</td>
            <td>{{$employee->firstname}}</td>
            <td>{{$employee->lastname}}</td>
            <td>{{$employee->email}}</td>
            <td class="text-center">
                <a href="employee/edit/<?php echo $employee->employee_id ?>" class="btn btn-success btn-sm">Edit</a>
              <a href="employee/deleteemp/<?php echo $employee->employee_id ?>" class="btn btn-success btn-sm">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection
