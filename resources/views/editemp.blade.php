@extends('layout')

@section('content')

<div class="mt-5">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  <table class="table">
    <tbody>
     <form method="post" action="{{ route('employee.update') }}" enctype="multipart/form-data">
	    <input type="hidden" name="id" value="{{ $employee[0]->employee_id }}"  >              
           
          <div class="form-group">
              @csrf
              <label for="name">First name</label>
              <input type="text" class="form-control" name="fname" value="{{ $employee[0]->firstname }}" />
          </div>
		   <div class="form-group">
              <label for="name">Last name</label>
              <input type="text" class="form-control" name="lname" value="{{ $employee[0]->lastname }}"/>
          </div>
         
		  <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" name="email" value="{{ $employee[0]->email }}"/>
          </div>
         
          <div class="form-group">
              <label for="profileimage">Profile image</label>
			  @if(!empty($employee[0]->profileimage))
					<img src="{{url('public/images/'.$employee[0]->profileimage)}}" width="100px" height="100px" class="rounded">
   @endif
              <input type="file" class="form-control" name="profileimage" />
          </div>
          <div class="form-group">
              <label for="phone">Birthdate</label>
              <input type="date" class="form-control" name="birthdate" value="{{ $employee[0]->brthdate }}"/>
          </div>
		   <div class="form-group">
              <label for="phone">Current Address</label>
              <input type="text" class="form-control" name="caddress" value="{{ $employee[0]->currentaddress }}"/>
          </div>
         
		  <div class="form-group">
              <label for="phone">Permenant Address</label>
              <input type="text" class="form-control" name="paddress" value="{{ $employee[0]->permenantaddress }}"/>
          </div>
          <div class="form-group">
              <label for="phone">Role</label>
          <select id="role" name="role[]" multiple class="form-control" >
      <option value="developer" <?php echo (isset($employee[0]->role) && in_array('developer', explode(",",$employee[0]->role) )) ? 'selected="selected"' : "" ?> >developer</option>
	 <option value="manager" <?php echo (isset($employee[0]->role) && in_array('manager', explode(",",$employee[0]->role) )) ? 'selected="selected"' : "" ?>>manager</option>
      <option value="tester" <?php echo (isset($employee[0]->role) && in_array('tester', explode(",",$employee[0]->role) )) ? 'selected="selected"' : "" ?>>tester</option>
      <option value="srdeveloper" <?php echo (isset($employee[0]->role) && in_array('srdeveloper', explode(",",$employee[0]->role) )) ? 'selected="selected"' : "" ?>>sr developer</option>
    
	</select>  </div>
         
          <button type="submit" class="btn btn-block btn-primary">Update</button>
      </form>
   </tbody>
  </table>
<div>
@endsection
