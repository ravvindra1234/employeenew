@extends('layout')

@section('content')

<div class="mt-5">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  <table class="table">
    <tbody>
     <form method="post" action="{{ route('employee.store') }}"enctype="multipart/form-data">
          <div class="form-group">
              @csrf
              <label for="name">First name</label>
              <input type="text" class="form-control" name="fname"/>
          </div>
		   <div class="form-group">
              <label for="name">Last name</label>
              <input type="text" class="form-control" name="lname"/>
          </div>
         
		  <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" name="email"/>
          </div>
         
          <div class="form-group">
              <label for="email">Profile image</label>
              
			   <input type="file" class="form-control" name="profileimage" placeholder="">
          </div>
          <div class="form-group">
              <label for="phone">Birthdate</label>
             <input type="date" class="form-control datepicker" id="birthdate"  name="birthdate" placeholder="">
                    
          </div>
		   <div class="form-group">
              <label for="phone">Current Address</label>
              <input type="text" class="form-control" name="caddress"/>
          </div>
         
		  <div class="form-group">
              <label for="phone">Permenant Address</label>
              <input type="text" class="form-control" name="paddress"/>
          </div>
          <div class="form-group">
              <label for="phone">Role</label>
               <select id="role" name="role[]" multiple class="form-control" >
      <option value="developer">developer</option>
      <option value="manager">manager</option>
      <option value="tester">tester</option>
      <option value="srdeveloper">sr developer</option>
      </select>
          </div>
         
          <button type="submit" class="btn btn-block btn-primary">Add</button>
      </form>
   </tbody>
  </table>
<div>
@endsection
