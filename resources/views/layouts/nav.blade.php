<?php /*
<div class="loading" style="background: #cccbcb;position: absolute;width: 100%;height: 100%;z-index: 9;">
    <img src="./images/myeventz-logo.webp" />
</div> */
?>
<style>
.navbar-soft .bottom .navbar-nav .megamenu.compact a{padding:1px 0}
</style>
<nav class="navbar navbar-hover navbar-expand-lg navbar-soft relative">
    <div class="top">
        <a class="navbar-brand" href="{{url('/')}}">
            <img class="logo img-fluid logo-main" src="{{url('public/frontend/images/myeventz-logo.png')}}" alt="MyEventz logo" />
            <img class="logo img-fluid logo-mobile" src="{{url('public/frontend/images/myeventz-logo.png')}}" alt="MyEventz logo" />
            <?php  // <img class="logo img-fluid logo-mobile" src="./images/crown-transparent.png" alt="MyEventz icon" /> ?>
        </a>
        <!-- <div class="header-call">
            <a class="phone" href="tel:+919372948377"><i class="fa fa-phone"></i> <span class="phone-num">+91 9372 948 377</span></a>
        </div> -->
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-navbar">
            <!-- <span class="navbar-toggler-icon"></span> -->
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="bottom collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item"> <a class="nav-link" href="{{url('/')}}">Home</a> </li>
            <!-- <li class="nav-item"><a class="nav-link" href="{{url('about-us')}}">About Us</a></li> -->
          
			<!-- <li class="nav-item"><a class="nav-link" href="{{url('ngo')}}">NGO</a></li> -->
        
            <!--<li class="nav-item dropdown has-megamenu">
                <a class="nav-link dropdown-toggle" href="{{url('venues')}}" data-toggle="dropdown">Venues</a>
                <div class="dropdown-menu animate fade-down megamenu" role="menu">
                    <div class="row row-wrapper">
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Destination Events</h6>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('venues/india/goa')}}">Goa </a></li>
                                    <li><a href="{{url('venues/category/rajasthan')}}">Rajasthan </a></li>
                                    <li><a href="{{url('venues/india/igatpuri')}}">Igatpuri </a></li>
                                    <li><a href="{{url('venues/india/lonavala')}}">Lonavala </a></li>
                                </ul>
                            </div>
                        </div>
						 <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">International Events</h6>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('venues')}}/filters?location%5B%5D=13">Maldives </a></li>
                                    <li><a href="{{url('venues')}}/filters?location%5B%5D=4">UAE </a></li>
                                    <li><a href="{{url('venues')}}/filters?location%5B%5D=3">Sri Lanka </a></li>
                                    <li><a href="{{url('venues')}}/filters?location%5B%5D=2">Turkey </a></li>
                                </ul>
                            </div>
                        </div>
                       
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Luxury Weddings</h6>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('venue/category/5-star-hotels')}}">Five star Wedding</a></li>
                                    <li><a href="{{url('venue/category/lawns')}}">Lawn Wedding</a></li>
                                    <li><a href="{{url('venue/category/resorts')}}">Resort Wedding</a></li>
                                    <li><a href="{{url('venue/category/banquet-halls')}}">Banquet Wedding</a></li>
                                    <li><a href="{{url('venue/category/turf')}}">Turf Wedding</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Theme Weddings</h6>
                                <ul class="list-unstyled">
                                    <li><a href="{{url('venue/category/beach-resort')}}">Beach Wedding</a></li>
                                    <li><a href="{{url('venues/category/palace')}}">Palace Wedding</a></li>
                                    <li><a href="{{url('venues/category/poolside')}}">Poolside Wedding</a></li>
                                    <li><a href="{{url('/')}}">Fairytale Wedding</a></li>
                                    <li><a href="#wpackages"> Wedding Packages</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>                
            </li>-->
		      <li class="nav-item"><a class="nav-link" href="{{url('venues')}}">Venues</a></li>
		    <li class="nav-item"><a class="nav-link" href="{{url('vendors')}}">Vendors</a></li>
   
   
   
    <!--<li class="nav-item  dropdown has-megamenu">
                    <a class="nav-link dropdown-toggle" href="{{url('vendors')}}" data-toggle="dropdown">Vendors</a>-->
                  <!--  <div class="dropdown-menu animate fade-down megamenu " role="menu">
                        <div class="row">
                            <div class="col-megamenu">
                                <ul class="list-unstyled">
                                    <li><a href="{{url('vendors/all/photographers')}}">Photographers</a></li>
                                    <li><a href="{{url('vendors/all/make-up-artist')}}">Make Up Artists</a></li>
                                     <li><a href="{{url('vendors/all/Djs')}}">Djs</a></li>
                                    <li><a href="{{url('vendors/all/Decor')}}">Decor</a></li>
                                    <li><a href="{{url('vendors/all/Anchors')}}">Anchors</a></li>
                                    <li><a href="{{url('vendors/all/Bridal - Designers')}}">Designers</a></li>
                                    <li><a href="{{url('vendors/all/Wedding - Cakes')}}">Wedding Cakes</a></li>
                                    <li><a href="{{url('vendors/all/Wedding-Invitations-digital-and-prints')}}">Wedding Invitations-digital and prints</a></li>
                              <li><a href="{{url('vendors/all/Gifts')}}">Gifts</a></li>
                                    
							  </ul>
                            </div>
                        </div>
                    </div>
               -->







      <!--<div class="dropdown-menu animate fade-down megamenu" role="menu">
                    <div class="row row-wrapper">
                        <div class="col-md-4 col-6">
                            <div class="col-megamenu">
                                <ul class="list-unstyled">
                            <li><a href="{{url('vendors/all/photographers')}}">Photographers</a></li>
                                    <li><a href="{{url('vendors/all/make-up-artist')}}">Make Up Artists</a></li>
                                     <li><a href="{{url('vendors/all/Djs')}}">Djs</a></li>
                                        </ul>
                            </div>
                        </div>
						 <div class="col-md-4 col-6">
                            <div class="col-megamenu">
                                <ul class="list-unstyled">
                               <li><a href="{{url('vendors/all/Decor')}}">Decor</a></li>
                                    <li><a href="{{url('vendors/all/Anchors')}}">Anchors</a></li>
                                    <li><a href="{{url('vendors/all/Bridal - Designers')}}">Designers</a></li>
                                     </ul>
                            </div>
                        </div>
                       
                        <div class="col-md-4 col-6">
                            <div class="col-megamenu">
                                <ul class="list-unstyled">
                          <li><a href="{{url('vendors/all/Wedding - Cakes')}}">Wedding Cakes</a></li>
                                    <li><a href="{{url('vendors/all/Wedding-Invitations-digital-and-prints')}}">Wedding Invitations</a></li>
                              <li><a href="{{url('vendors/all/Gifts')}}">Gifts</a></li>
                                          </ul>
                            </div>
                        </div>
						
						
                    </div>
                </div>-->                
          


















			   </li>
            
   
   
   
   
   
   
   
   
   
   
      	    <li class="nav-item"><a class="nav-link" href="{{url('blogs')}}">Blogs</a></li>
          
            <?php
            /*
            <li class="nav-item"><a class="nav-link" href="#">Venues</a></li>
            <li class="nav-item dropdown has-megamenu">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Vendors</a>
                <div class="dropdown-menu animate fade-down megamenu" role="menu">
                    <div class="row row-wrapper">
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Title Menu One</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Title Menu Two</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Title Menu Three</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="col-megamenu">
                                <h6 class="title">Title Menu Four</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                    <li><a href="#">Submenu item</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            */
            ?>
            <li class="nav-item"><a class="nav-link" href="{{url('reviews')}}">Reviews</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('contact')}}">Contact</a></li>
            <!-- @if(Session::get('user_id') !== null)
            @else
                <li class="nav-item"><a class="nav-link" href="{{url('register')}}">Register</a></li>
            @endif -->        
            <!-- <li class="nav-item"><a class="nav-link" href="./wishlist.php">My Wishlist</a></li>
            <li class="nav-item"><a href="#" class="nav-link requestcompareBox" data-toggle="modal" data-target="#compareBoxModal">Compare</a></li>
            <li class="nav-item"><a class="nav-link" href="./profile.php">Profile</a></li> -->
        </ul>
        @if(Session::get('user_id') !== null || Session::get('vendor_id') !== null)
        @else
            <ul class="navbar-nav ml-auto rightened">
                <!-- <li class="nav-item user-profile"><a class="nav-link" href="{{url('register')}}"><i class="fa fa-user-o"></i>Login / Register</a></li> -->
                <li class="nav-item user-profile dropdown has-megamenu">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" alt="net" title="title"><!--<i class="fa fa-user-o"></i>-->Login / Register</a>
                    <div class="dropdown-menu animate fade-down megamenu compact" role="menu">
                        <div class="row row-wrapper">
                            <div class="col-megamenu">
                                <ul class="list-unstyled">
                                    <li><a href="{{url('register')}}">Customer</a></li>
                                    <li><a href="{{url('register-vendor')}}">Vendor</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        @endif
        @if(Session::get('user_id') !== null || Session::get('vendor_id') !== null)
        <ul class="navbar-nav ml-auto rightened">
            <li class="nav-item user-profile dropdown has-megamenu">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" alt=""><!--<i class="fa fa-user-o"></i>-->My Account</a>
                <div class="dropdown-menu animate fade-down megamenu compact" role="menu">
                    <div class="row row-wrapper">
                        <div class="col-megamenu">
                            <ul class="list-unstyled">
                                <li><a href="{{url('my-profile')}}">My Profile</a></li>
                                <li><a href="{{url('shortlist')}}">My Shortlist</a></li>
                                <li><a href="{{url('my-transactions')}}">My Payments</a></li>
                                <li><a href="#" class="requestcompareBox" data-toggle="modal" data-target="#compareBoxModal">Compare</a></li>
                                <li><a href="{{url('log-out')}}">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        @endif
    </div>
</nav>

<!-- <?php //include('./callback-popup.php'); ?>
<?php //include('./compare-popup.php'); ?> -->