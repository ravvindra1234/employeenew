<!DOCTYPE html>
<html lang="en">

<head>
<?php
//var_dump(Session);
//echo Session::get('user_id');
?>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <meta name="viewport"
        content="width=device-width, shrink-to-fit=no, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" href="{{url('public/frontend/images/favicon.png')}}" type="image/x-icon" />
    <script>
        var SITEURL='{{URL::to('')."/"}}';
    </script>
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900&display=swap" rel="stylesheet"> -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;1,300;1,400;1,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/fontawesome.min.css?v=2.1')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/animate.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/menukit.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/autocomplete.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/easy-autocomplete.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/easy-autocomplete.themes.min.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/owl.theme.default.min.css?v='.time())}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/owl.carousel.min.css?v='.time())}}" />

    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/main.css?v='.time())}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/style.css?v='.time())}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('public/frontend/css/toastr.min.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" integrity="sha512-/Ae8qSd9X8ajHk6Zty0m8yfnKJPlelk42HTJjOHDWs1Tjr41RfsSkceZ/8yyJGLkxALGMIYd5L2oGemy/x1PLg==" crossorigin="anonymous" />
    <script src="{{url('public/frontend/js/jquery-3.3.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" integrity="sha512-2xXe2z/uA+2SyT/sTSt9Uq4jDKsT0lV4evd3eoE/oxKih8DSAsOF6LUb+ncafMJPAimWAXdu9W+yMXGrCVOzQA==" crossorigin="anonymous"></script>
    <!-- <script src="{{url('public/frontend/js/bundle.js')}}"></script> -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TRD6GV7');
    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

    <meta name="theme-color" content="#092724" />
    <meta name="theme-color" content="#092724" />
    <style>
        .toast-top-center {
            margin-top: 4rem !important;
        }
        .g-recaptcha div {
            margin: -.5rem -2rem;
        }
    </style>