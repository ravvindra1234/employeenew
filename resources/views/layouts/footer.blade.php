<section class="section-callBack">
    <button type="button" class="btn btn-secondary requestCallBack" data-toggle="modal" data-target="#callBackModal">
        Request call back
    </button>
    <div class="modal light fade" id="callBackModal" tabindex="-1" role="dialog" aria-labelledby="callBackModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <h5 class="modal-title" id="callBackModalLabel">TALK TO AN <strong> EVENT EXPERT</strong></h5>
                        <p class="modal-desc">get a free consultation</p>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="myclose">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="popup-form" method="post" action="{{url('thank-you')}}" onsubmit="return gcaptcha001();">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name" name="name" value="{{(Session::get('user_id') !== null) ? Session::get('user_name') : ''}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{(Session::get('user_id') !== null) ? Session::get('user_email') : ''}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" name="phone" maxlength="10" value="{{(Session::get('user_id') !== null) ? Session::get('user_mobile') : ''}}"
                                required
                                onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="acceptTerms">
                                <label class="custom-control-label" for="acceptTerms">Share my number with
                                    vendors</label>
                            </div>
                        </div>
                        <input type="hidden" name="lead_type" value="">
                        <div class="form-group" style="text-align: center;">
                            <div class="g-recaptcha" id="gcaptcha001" style="display: inline-block;"></div>
                            <div class="g-recaptcha001-error"></div>
                        </div>
                        <button type="submit" class="btn btn-primary disableFlag"><i class="fa fa-phone"></i>Request a
                            Callback</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal light fade" id="bestOfferModal" tabindex="-1" role="dialog" aria-labelledby="bestOfferModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <h5 class="modal-title" id="bestOfferModalLabel">TALK TO AN <strong> EVENT EXPERT</strong></h5>
                        <p class="modal-desc">Get more details for this venue</p>
                    </div>
                    <button type="button" class="close ctm_res_close" data-dismiss="modal" aria-label="Close" onclick="return localStorage.setItem('popup', '');">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="popup-form" method="POST" action="{{url('offer-popup')}}" onsubmit="return gcaptcha002();">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name" name="name" value="{{(Session::get('user_id') !== null) ? Session::get('user_name') : ''}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{(Session::get('user_id') !== null) ? Session::get('user_email') : ''}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" name="phone" maxlength="10"
                                onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="{{(Session::get('user_id') !== null) ? Session::get('user_mobile') : ''}}"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Venue Name" name="venueName"
                                id="venueName" value="" required>
                        </div>
                        <input type="hidden" name="venueId" id="venueId" value="">
                        <input type="hidden" name="source" id="source" value="">

                        <div class="form-group">
                            <input type="date" class="form-control" placeholder="Date" name="date"
                                value="{{ date('Y-m-d') }}" required>
                        </div>
                        <div class="form-group" style="text-align: center;">
                            <div class="g-recaptcha" id="gcaptcha002" style="display: inline-block;"></div>
                            <div class="g-recaptcha002-error"></div>
                        </div>
                        <button type="submit" class="btn btn-primary disableFlag"><i class="fa fa-phone"></i>Request a
                            Callback</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal light fade" id="viewMoreFromBooking" tabindex="-1" role="dialog" aria-labelledby="viewMoreFromBookingLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <!-- <h5 class="modal-title" id="viewMoreFromBookingLabel">TALK TO AN <strong> EVENT EXPERT</strong></h5> -->
                        <p class="modal-desc">More Details About Booking</p>
                    </div>
                    <button type="button" class="close ctm_res_close" data-dismiss="modal" aria-label="Close" onclick="return localStorage.setItem('popup', '');">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <p>Event Name: <small class="eventname"></small></p>
                            <p>Event Time: <small class="eventtime"></small></p>
                            <p>Event Date: <small class="eventdate"></small></p>
                            <p>Event Type: <small class="eventtype"></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- for vendor page -->
    <div class="modal light fade" id="bestOfferModalVendor" tabindex="-1" role="dialog" aria-labelledby="bestOfferModalLabelVendor"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <h5 class="modal-title" id="bestOfferModalLabelVendor">TALK TO AN <strong> EVENT EXPERT</strong></h5>
                        <p class="modal-desc">Get more details for this vendor</p>
                    </div>
                    <button type="button" class="close ctm_res_close" data-dismiss="modal" aria-label="Close" onclick="return localStorage.setItem('popup', '');">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="popup-form" method="POST" action="{{url('offer-popup-vendor')}}" onsubmit="return gcaptcha003();">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name" name="name" value="{{(Session::get('user_id') !== null) ? Session::get('user_name') : ''}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{(Session::get('user_id') !== null) ? Session::get('user_email') : ''}}" required>
                        </div>
                        @if(Session::get('user_id') !== null)
                        <input type="hidden" name="customer_id" value="Session::get('user_id')">
                        @endif
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" name="mobile" maxlength="10"
                                onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="{{(Session::get('user_id') !== null) ? Session::get('user_mobile') : ''}}"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Vendor Name" name="vendorName"
                                id="vendorName" value="" required>
                        </div>
                        <input type="hidden" name="vendorId" id="vendorId" value="">
                        <input type="hidden" name="source" id="source" value="">
                        <input type="hidden" name="vendorEmail" id="vendorEmail" value="">
                        <div class="form-group">
                            <input type="date" class="form-control" placeholder="Date" name="event_date"
                                value="{{-- date('Y-m-d') --}}" required>
                        </div>
                        <div class="form-group" style="text-align: center;">
                            <div class="g-recaptcha" id="gcaptcha003" style="display: inline-block;"></div>
                            <div class="g-recaptcha003-error"></div>
                        </div>
                        <button type="submit" class="btn btn-primary disableFlag"><i class="fa fa-phone"></i>Request a
                            Callback</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Forgot password modal -->
    <div class="modal light fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <h5 class="modal-title" id="forgotPasswordModalLabel">FORGOT <strong> PASSWORD</strong></h5>
                        <p class="modal-desc">reset your password here</p>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="myclose">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="popup-form forgotPasswordForm">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your registered mobile number" id="forgotPhone" name="mobile_no" maxlength="10"
                                required
                                onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                        </div>
                        <input type="hidden" name="from" id="from" value="{{request()->segment(1)}}">
                        <button type="submit" class="btn btn-primary" id="forgotPasswordBtn"><i class="fa fa-paper-plane"></i>Send My Password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Sent Shortlist Modal -->
    <div class="modal light fade" id="sentShortlistedVenuesModal" tabindex="-1" role="dialog" aria-labelledby="sentShortlistedVenuesModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-headings">
                        <h5 class="modal-title" id="sentShortlistedVenuesModalLabel">SEND SHORTLISTED <strong> MAIL</strong></h5>
                        <p class="modal-desc">send what you shorlisted via mail</p>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="myclose">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="popup-form" action="{{url('share-mail-of-shorlited-venues')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="eg. xyz@mail.com, jon@gmail.com" id="emailIds" name="emailIds" required="">
                            <p class="text-center"><small><i>Note: You can enter only four mail ids</i></small></p>  
                        </div>
                        <input type="hidden" name="userid" id="userid" value="">
                        <input type="hidden" name="username" id="username" value="">
                        <button type="submit" class="btn btn-primary" id=""><i class="fa fa-paper-plane"></i>Send Mail</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-contactNumber">
    <div class="text-center top">
        <div class="icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.479 50.479">
                <path class="svg-none" d="M0,50.479V0H50.479V50.479Z" />
                <g transform="translate(6.014 2.958)">
                    <path class="svg-primary"
                        d="M46.15,13.242a16.354,16.354,0,0,0-16.366,15.42,3.712,3.712,0,0,0-2.8,4.259l1.341,7.532a3.724,3.724,0,0,0,3.667,3.076,3.392,3.392,0,0,0,.631-.04l3.195-.552a1.178,1.178,0,0,0,.946-1.38l-1.617-9.111a4.674,4.674,0,0,1,2.405-2.405c1.656-.513,3.944.671,6.586,2.051,3.076,1.617,6.822,3.549,11.121,3.549.473,0,.946-.04,1.42-.079l-.946,6.113v.039C52.1,49.8,47.806,50.668,46.11,50.668c-1.656,0-4.89-.789-8.045-5.955a1.177,1.177,0,1,0-2.011,1.222c3.747,6.192,7.927,7.1,10.056,7.1,6.034,0,9.859-6.349,11.555-9.859l2.012.315a3.391,3.391,0,0,0,.631.04,3.724,3.724,0,0,0,3.667-3.076l1.341-7.532a3.734,3.734,0,0,0-2.8-4.259A16.41,16.41,0,0,0,46.15,13.242Zm.039,2.288A14.062,14.062,0,0,1,60.15,28.15l-.631-.119c-1.656-7.02-6.428-11.082-13.369-11.082a12.684,12.684,0,0,0-12.581,8.715,1.2,1.2,0,1,0,2.248.828c1.7-4.693,5.284-7.3,10.332-7.177,9.78.237,8.6,13.764,8.6,13.764-4.5.67-6.034-1.341-9.5-3.155-3.116-1.617-5.8-3.037-8.4-2.209a5.72,5.72,0,0,0-2.248,1.42l-.039-.2a1.09,1.09,0,0,0-.473-.749,1.186,1.186,0,0,0-.868-.2l-.986.158A14.033,14.033,0,0,1,46.189,15.53ZM32.425,30.594,34.2,40.809l-2.011.355c-.079,0-.158.039-.236.039a1.355,1.355,0,0,1-1.341-1.144L29.27,32.527a1.354,1.354,0,0,1,1.1-1.577Zm27.449,0,2.011.355a1.353,1.353,0,0,1,1.1,1.577l-1.34,7.532A1.355,1.355,0,0,1,60.308,41.2a.5.5,0,0,1-.236-.039l-1.815-.316ZM41.259,52.64a.144.144,0,0,0-.118.039H41.1c-.039,0-.039,0-.079.039h-.079l-8.716,3.549a1.159,1.159,0,0,0-.631,1.538,1.219,1.219,0,0,0,1.144.789,1.417,1.417,0,0,0,.434-.079L40.786,55.4a6.1,6.1,0,0,0,10.727,0l7.612,3.116a.923.923,0,0,0,.434.079,1.193,1.193,0,0,0,.473-2.288l-8.715-3.549a1.192,1.192,0,0,0-1.538.632c0,.039-.039.078-.039.118a3.727,3.727,0,0,1-7.177,0c0-.039,0-.039-.039-.079v-.079a.039.039,0,0,0-.04-.039c0-.039-.039-.078-.039-.118a.085.085,0,0,0-.079-.079c-.039-.039-.04-.079-.079-.079l-.079-.079-.079-.079c-.039-.039-.079-.039-.079-.079a.136.136,0,0,1-.079-.04c-.039,0-.079-.039-.119-.039s-.078-.039-.118-.039a.145.145,0,0,1-.119-.039h-.355Z"
                        transform="translate(-26.924 -13.242)" />
                </g>
            </svg>
        </div>
        <hr />
    </div>
    <div class="text-center">
        <?php // if($ngoPage) {
    //     echo '
    //     <h3 class="title">A certain contribution from every event will be shared with Mumbai Roti Bank</h3>
    //     <p class="info">MyEventz is here to assist 25 x 366 days</p>';
    // }
    // else {
    //     echo '
    //     <h3 class="title">Talk to an Event Expert for Free</h3>
    //     <p class="info">MyEventz is here to assist 25 x 366 days</p>';
    // }
    ?>
        <a href="tel:+919372948377" class="number">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.724 16.724">
                <path class="svg-none" d="M0,16.724V0H16.724V16.724Z" />
                <g transform="translate(1.394 1.671)">
                    <path class="svg-green"
                        d="M18.514,17.189c.071,0,.071,0,.139.071s.28.278.28.419a3.483,3.483,0,0,0,.068.765c.139.348.28,1.255.487,1.742s.28.768.071,1.045A20.931,20.931,0,0,1,18.027,22.9a5.1,5.1,0,0,0,.765,1.113,8.929,8.929,0,0,0,1.6,1.742,9.092,9.092,0,0,0,2.229,1.532c.768-.765,1.394-1.394,1.6-1.532s.21-.21.558-.068a5.494,5.494,0,0,0,1.394.416c.487.071,1.045.139,1.394.21s.348.071.419.21a.764.764,0,0,1,.207.416V29.8a.531.531,0,0,1-.068.348,2.71,2.71,0,0,0-.139.487c0,.071-.348.21-.558.21a12.261,12.261,0,0,1-1.464-.071,13.919,13.919,0,0,1-2.3-.555A13.341,13.341,0,0,1,19.63,28.06a14.325,14.325,0,0,1-3.9-4.807,12.812,12.812,0,0,1-1.255-4.042,7.613,7.613,0,0,1-.139-1.394.966.966,0,0,1,.419-.487,1.293,1.293,0,0,1,.555-.142h3.206Z"
                        transform="translate(-14.333 -17.189)" />
                </g>
            </svg>
            <span>Call +91 937 294 8377</span>
        </a>
    </div>
</section>

<div class="links-cloud">
    <div class="container">
        <h3 class="footer-title">Wedding Venues </h3>
        <div class="row footer-list-wrap">
            <div class="col col-xs-3 footer-list">
                <p class="footer-list-title"><a class="strong" href="#">Mumbai </a></p>
                <ul>
                    <li><h4><a href="{{url('venues/india/mumbai')}}">Wedding Venues in Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/mumbai')}}?_token=EI4z33C2J9yacB4WrQqJACj8hqqga2dDJjGDeDlm&location%5B%5D=1&venueType%5B%5D=1">5 Star Venues in Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/mumbai')}}?_token=EI4z33C2J9yacB4WrQqJACj8hqqga2dDJjGDeDlm&location%5B%5D=1&venueType%5B%5D=2">Banquet Venues in Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/mumbai')}}?_token=EI4z33C2J9yacB4WrQqJACj8hqqga2dDJjGDeDlm&location%5B%5D=1&venueType%5B%5D=6">Lawn Venues in Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/mumbai')}}?_token=EI4z33C2J9yacB4WrQqJACj8hqqga2dDJjGDeDlm&location%5B%5D=1&venueType%5B%5D=3">Lounge Venues in Mumbai</a></h4></li>
                </ul>
				
			    <p class="footer-list-title"><a class="strong" href="#">Best Wedding Packages </a></p>
                <ul>
                    <li><h4><a href="{{url('venues/india/mumbai')}}?_token=EI4z33C2J9yacB4WrQqJACj8hqqga2dDJjGDeDlm&location%5B%5D=1&venueType%5B%5D=1">All inclusive weddings in 5 lacs</a></h4></li>
                    <li><h4><a href="{{url('venue/category/drivable-destination')}}">Destination Weddings in 10 lacs</a></h4></li>
                    <li><h4><a href="{{url('venues/india/goa')}}">Goa Weddings starting at 15 lacs</a></h4></li>
                </ul>
				
				</div>
				<div class="col col-xs-3 footer-list">
         
                <p class="footer-list-title"><a class="strong" href="#">Venues by City
                        </a></p>
                <ul>
                    <li><h4><a href="{{url('venues/india/mumbai')}}">Wedding Venues in Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/goa')}}">Wedding Venues in Goa</a></h4></li>
                    <li><h4><a href="{{url('venues/india/jaipur')}}">Wedding Venues in Jaipur</a></h4></li>
                    <li><h4><a href="{{url('venues/india/lonavala')}}">Wedding Venues in Lonavala</a></h4></li>
					
                      <li><h4><a href="{{url('venues/india/navi-mumbai')}}">Wedding Venues in Navi Mumbai</a></h4></li>
                    <li><h4><a href="{{url('venues/india/pune')}}">Wedding Venues in Pune</a></h4></li>
                  



					</ul>
					

   <p class="footer-list-title"><a class="strong" href="#">Vendors
                        </a></p>
                <ul>
                    <li><h4><a href="{{url('vendors/')}}/all/photographers">Photographers</a></h4></li>
                    <li><h4><a href="{{url('vendors/')}}/all/make-up-artist">Make Up Artists</a></h4></li>
                    <li><h4><a href="{{url('vendors/')}}/all/Djs">Djs</a></h4></li>
                    <li><h4><a href="{{url('vendors/')}}/all/Decor">Decor</a></h4></li>
					
                      <li><h4><a href="{{url('vendors/')}}/all/Anchors">Anchors</a></h4></li>
                    <li><h4><a href="{{url('vendors/')}}/all/Performance-artists">Performance Artists</a></h4></li>
                  
				    <li><h4><a href="{{url('vendors/')}}/all/Wedding-Invitations-digital-and-prints">Wedding Invitations-Digital and Prints</a></h4></li>
                    <li><h4><a href="{{url('vendors/')}}/all/Gifts">Gifts</a></h4></li>
                  



					</ul>
			










					
					  </div>
				   <div class="col col-xs-3 footer-list">
         
                <p class="footer-list-title"><a class="strong" href="#">Destination Venues</a>
                </p>
                <ul>
                    <li><h4><a href="{{url('venues/india/goa')}}">Goa Destination Wedding</a></h4></li>
                    <li><h4><a href="{{url('venues/india/jaipur')}}">Jaipur Destination Wedding</a></h4></li>
					  <li><h4><a href="{{url('venues/india/udaipur')}}">Udaipur Destination Wedding</a></h4></li>
					  <li><h4><a href="{{url('venue/category/5-star-hotels')}}">Drivable Destination Wedding</a></h4></li>
                  
					
					   <li><h4><a href="{{url('venues/india/lonavala')}}">Lonavala Destination Wedding</a></h4></li>
                    <li><h4><a href="{{url('venues/india/karjat')}}">Karjat Destination Wedding</a></h4></li>
                 
				 </ul>
				 
				   <p class="footer-list-title"><a class="strong" href="#">International Destination Venues</a>
                </p>
              <ul>
                    <li><h4><a href="{{url('venues')}}/filters?location%5B%5D=13">International Destination Wedding Maldives</a></h4></li>
					
					  <li><h4><a href="{{url('venues/thailand/phuket')}}">International Destination Wedding Phuket</a></h4></li>
					 <li><h4><a href="{{url('venues')}}/filters?location%5B%5D=3">International Destination Wedding Sri Lanka</a></h4></li>
					
						<li><h4><a href="{{url('venue/category/cruise')}}">Cruise Weddings</a></h4></li>
                    
                </ul>
				</div>
				 		   <div class="col col-xs-3 footer-list">
         
                <p class="footer-list-title"><a class="strong" href="#">Venues by Category
                        </a></p>
                <ul>
                    <li><h4><a href="{{url('venue/category/5-star-hotels')}}">5 Star Hotels</a></h4></li>
                    <li><h4><a href="{{url('venue/category/banquet-halls')}}">Banquet Halls</a></h4></li>
                    <li><h4><a href="{{url('venue/category/lounges-resto-bar')}}">Lounges Resto Bar</a></h4></li>
                    <li><h4><a href="{{url('venue/category/lawns')}}">Lawns</a></h4></li>
                      <li><h4><a href="{{url('venue/category/hotels')}}">Hotels</a></h4></li>
                    <li><h4><a href="{{url('venue/category/terrace')}}">Terrace </a></h4></li>
                    <li><h4><a href="{{url('venue/category/resorts')}}">Resorts</a></h4></li>
                    <li><h4><a href="{{url('venue/category/heritage-venue')}}">Heritage Venue</a></h4></li>
                       <li><h4><a href="{{url('venue/category/beach-resort')}}">Beach Resort</a></h4></li>
                 
					</ul>
					  
					  </div>
				 
         
            </div>
      






    





	  </div>
    </div>
</div>

<footer class="footer">
    <div>
        <div class="container">
            <div class="d-flex justify-content-between align-items-start flex-wrap">
                <div class="flex-grow-0 d-flex flex-wrap left-footer">
                    <div class="footer-column">
                        <h3 class="title">Information</h3>
                        <a href="{{url('press-media')}}" class="footer-link"><span>Press & Media</span></a>
                        <a href="{{url('about-us')}}" class="footer-link"><span>About Us</span></a>
                        <a href="{{url('contact')}}" class="footer-link"><span>Get in Touch</span></a>
                        <a href="{{url('ngo')}}" class="footer-link"><span>NGO</span></a>
                        <a href="{{url('blogs')}}" class="footer-link"><span>Blogs</span></a>
                        <a href="{{url('reviews')}}" class="footer-link"><span>Reviews</span></a>
                        <a href="{{url('online-events')}}" class="footer-link"><span>Online Events</span></a>

                    </div>
                    <div class="footer-column">
                        <h3 class="title">Important Links</h3>
                        <!-- <a href="{{url('venues')}}" class="footer-link"><span>Venues</span></a>-->
					    
                        <a href="{{url('venues/india/mumbai')}}" class="footer-link"><span>Venues in Mumbai</span></a>
                        <a href="{{url('venues/india/goa')}}" class="footer-link"><span>Goa Destination
                                Wedding</span></a>
                        <a href="{{url('venues/india/lonavala')}}" class="footer-link"><span>Lonavala Destination
                                Wedding</span></a>
                        <a href="{{url('venues/india/jaipur')}}" class="footer-link"><span>Jaipur Destination
                                Wedding</span></a>
                        <a href="{{url('venue/category/5-star-hotels')}}" class="footer-link"><span>5 Star
                                Wedding</span></a>
                        <a href="{{url('venue/category/resorts')}}" class="footer-link"><span>Resort Wedding</span></a>
                        <a href="{{url('venue/category/banquet-halls')}}" class="footer-link"><span>Banquet
                                Wedding</span></a>
                        <a href="{{url('venue/category/lawns')}}" class="footer-link"><span>Lawn Wedding</span></a>
                        <a href="{{url('venue/category/cruise')}}" class="footer-link"><span>Cruise Wedding</span></a>
                        <a href="{{url('#')}}" class="footer-link"><span>Deals & Offers</span></a>

                    </div>
                    <div class="footer-column">
                        <h3 class="title">Legal Pages</h3>
                        <a href="{{url('privacy-policy')}}" class="footer-link"><span>Privacy Policy</span></a>
                        <a href="{{url('privacy-policy')}}" class="footer-link"><span>Terms and Conditions</span></a>

                    </div>
                </div>
                <div class="flex-shrink-0 footer-column">
                    <h3 class="title">Follow us on</h3>
                    <a href="https://www.facebook.com/MyEventz-110129537346411/" target="_blank"
                        class="social-link footer-link">
                        <div class="svg-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="10" viewBox="0 0 9.734 17.151">
                                <path class="svg-primary"
                                    d="M6.513,17.15H3.766a.833.833,0,0,1-.832-.832V10.123h-1.6A.833.833,0,0,1,.5,9.291V6.637A.833.833,0,0,1,1.332,5.8h1.6V4.476a4.526,4.526,0,0,1,1.2-3.243A4.311,4.311,0,0,1,7.309,0L9.4,0a.833.833,0,0,1,.831.832V3.3a.833.833,0,0,1-.832.832H7.993c-.43,0-.54.086-.563.113-.039.044-.085.168-.085.51V5.8H9.3a.846.846,0,0,1,.412.1.835.835,0,0,1,.428.728V9.292a.833.833,0,0,1-.832.832H7.345v6.2a.833.833,0,0,1-.832.832ZM3.94,16.145h2.4V9.673a.556.556,0,0,1,.555-.555H9.13V6.81H6.895a.556.556,0,0,1-.555-.555v-1.5a1.712,1.712,0,0,1,.336-1.175,1.7,1.7,0,0,1,1.316-.453H9.229V1.008l-1.92,0A3.174,3.174,0,0,0,3.94,4.476V6.255a.556.556,0,0,1-.555.555H1.506V9.118H3.385a.556.556,0,0,1,.555.555ZM9.4,1.008h0Zm0,0"
                                    transform="translate(-0.5 0)" />
                            </svg>
                        </div>
                        <span>Facebook</span>
                    </a>
                    <a href="https://www.linkedin.com/company/my-eventz" target="_blank"
                        class="social-link footer-link">
                        <div class="svg-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" viewBox="0 0 16.288 16.25">
                                <path class="svg-primary"
                                    d="M4.209,16.584H.256V5.489H4.209ZM.982,15.859h2.5V6.214H.982ZM2.234,4.8A2.231,2.231,0,1,1,4.466,2.565,2.235,2.235,0,0,1,2.234,4.8Zm0-3.737A1.506,1.506,0,1,0,3.741,2.565,1.509,1.509,0,0,0,2.234,1.059ZM16.288,16.584H12.34v-5.4c0-1.618-.2-2.387-1.314-2.387s-1.572.645-1.572,2.3v5.493H5.508V5.489H9.325v.785a3.926,3.926,0,0,1,2.733-1.042c3.728,0,4.23,2.681,4.23,5.3v6.05Zm-3.222-.725h2.5V10.534c0-2.826-.586-4.578-3.5-4.578A3.037,3.037,0,0,0,9.327,7.438l-.1.194H8.6V6.214H6.234v9.645h2.5V11.092c0-.827,0-3.025,2.3-3.025,2.04,0,2.04,2.025,2.04,3.113v4.68h0Z"
                                    transform="translate(0 -0.334)" />
                            </svg>
                        </div>
                        <span>Linkedin</span>
                    </a>
                    <a href="https://www.youtube.com/channel/UCdtd-wObtJHjOu-n0kjKNqA" target="_blank"
                        class="social-link footer-link">
                        <div class="svg-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" viewBox="0 0 22.358 15.996">
                                <path class="svg-primary"
                                    d="M11.715,63.6h0c-2.543,0-5.311-.08-8.227-.237a3.478,3.478,0,0,1-3.249-3.23,43.134,43.134,0,0,1,0-9.062,3.5,3.5,0,0,1,3.246-3.229,124.811,124.811,0,0,1,15.389,0,3.463,3.463,0,0,1,3.247,3.227,43.235,43.235,0,0,1,0,9.062,3.467,3.467,0,0,1-3.243,3.231C16.668,63.522,14.258,63.6,11.715,63.6Zm-.595-15.138c-2.534,0-5.086.08-7.583.237a2.621,2.621,0,0,0-2.445,2.462,42.287,42.287,0,0,0,0,8.882,2.6,2.6,0,0,0,2.442,2.463c2.9.156,5.653.236,8.18.236s4.912-.079,7.1-.236a2.631,2.631,0,0,0,2.449-2.461,42.3,42.3,0,0,0,0-8.882A2.626,2.626,0,0,0,18.821,48.7C16.246,48.544,13.655,48.465,11.121,48.465Zm-2.128,4.1v5.894l5.66-2.927Z"
                                    transform="translate(0 -47.606)" />
                            </svg>
                        </div>
                        <span>YouTube</span>
                    </a>
                    <a href="https://twitter.com/My_Eventz?s=09" target="_blank" class="social-link footer-link">
                        <div class="svg-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20.795" viewBox="0 0 20.795 17.02">
                                <path class="svg-primary"
                                    d="M20.795,2.273c-1.308.06-1.28.055-1.427.068L20.142.129s-2.418.89-3.031,1.049A4.852,4.852,0,0,0,11.4.718a3.957,3.957,0,0,0-1.9,4.069A10.555,10.555,0,0,1,2.656.937L2.083.248l-.429.787a4.757,4.757,0,0,0-.507,3.2,4.6,4.6,0,0,0,.446,1.25L1.1,5.3l-.058.822a4.388,4.388,0,0,0,.744,2.608,4.638,4.638,0,0,0,.58.715L2.112,9.4l.31.942a4.581,4.581,0,0,0,2.355,2.728c-1.1.466-1.985.763-3.443,1.243L0,14.756l1.232.674A12.933,12.933,0,0,0,5,16.8c3.646.572,7.751.106,10.514-2.383,2.328-2.1,3.091-5.078,2.933-8.181a1.939,1.939,0,0,1,.362-1.262c.516-.689,1.981-2.7,1.984-2.7ZM17.837,4.245a3.157,3.157,0,0,0-.6,2.054c.16,3.13-.692,5.557-2.531,7.214-2.149,1.936-5.616,2.7-9.511,2.085a9.432,9.432,0,0,1-2.038-.606,32.312,32.312,0,0,0,3.691-1.517l2.128-1.007-2.35-.15a3.323,3.323,0,0,1-2.639-1.5,4.214,4.214,0,0,0,.894-.149l2.241-.624-2.26-.553A3.366,3.366,0,0,1,2.8,8.059a3.447,3.447,0,0,1-.446-.983,5.641,5.641,0,0,0,.917.146l2.092.207L3.709,6.136A3.488,3.488,0,0,1,2.387,2.461,12.122,12.122,0,0,0,10.93,6.137,7.214,7.214,0,0,0,10.8,5.1a2.8,2.8,0,0,1,1.23-3.349,3.639,3.639,0,0,1,4.293.356,1.09,1.09,0,0,0,1,.268,8.421,8.421,0,0,0,.824-.251l-.525,1.5H18.3l-.461.617Zm0,0"
                                    transform="translate(0 0)" />
                            </svg>
                        </div>
                        <span>Twitter</span>
                    </a>
                    <a href="https://www.instagram.com/my.eventz/?igshid=1spxsn07r1mkv" target="_blank"
                        class="social-link footer-link">
                        <div class="svg-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20.322" viewBox="0 0 20.322 20.322">
                                <path class="svg-primary"
                                    d="M14.714,0H5.608A5.614,5.614,0,0,0,0,5.608v9.106a5.614,5.614,0,0,0,5.608,5.608h9.106a5.614,5.614,0,0,0,5.608-5.608V5.608A5.614,5.614,0,0,0,14.714,0Zm3.805,14.714a3.809,3.809,0,0,1-3.805,3.805H5.608a3.809,3.809,0,0,1-3.8-3.805V5.608A3.809,3.809,0,0,1,5.608,1.8h9.106a3.809,3.809,0,0,1,3.805,3.805v9.106Z" />
                                <path class="svg-primary"
                                    d="M46.2,40.97a5.236,5.236,0,1,0,5.236,5.236A5.242,5.242,0,0,0,46.2,40.97Zm0,8.669a3.433,3.433,0,1,1,3.433-3.433A3.437,3.437,0,0,1,46.2,49.639Z"
                                    transform="translate(-36.044 -36.045)" />
                                <path class="svg-primary"
                                    d="M120.243,28.251a1.321,1.321,0,1,0,.935.387A1.327,1.327,0,0,0,120.243,28.251Z"
                                    transform="translate(-104.626 -24.855)" />
                            </svg>
                        </div>
                        <span>Instagram</span>
                    </a>
                </div>

            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="d-md-flex d-block justify-content-between justify-content-center align-items-center content">
                <h4 class="rights flex-grow-1">&copy; 2020 My Eventz All rights reserved</h4>
                <div class="legal-pages flex-shrink-1 d-flex justify-content-start align-items-start flex-wrap">
                    <a href="{{url('ngo')}}" class="legal-page">Community</a>
                    <a href="{{url('privacy-policy')}}" class="legal-page">Terms & Conditions</a>
                    <a href="{{url('privacy-policy')}}" class="legal-page">Privacy Policy</a>
                </div>
            </div>
        </div>
    </div>
</footer>


<div class="whatsapp-cta-wrap"></div>
<script src="{{url('public/frontend/js/popper.min.js')}}"></script>
<script src="{{url('public/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/frontend/js/jquery.waypoints.min.js')}}"></script>
<script src="{{url('public/frontend/js/menukit.js?v=1.2')}}"></script>
<script src="{{url('public/frontend/js/select2.min.js')}}"></script>
<script src="{{url('public/frontend/js/typed.min.js')}}"></script>
<script src="{{url('public/frontend/js/floating-wa.min.js')}}"></script>
<script src="{{url('public/frontend/js/autocomplete.js')}}"></script>
<script src="{{url('public/frontend/js/jquery.easy-autocomplete.min.js')}}"></script>
<script src="{{url('public/frontend/js/magnific-popup.min.js')}}"></script>
<script src="{{url('public/frontend/js/jquery.unveil2.min.js')}}"></script>

<script src="{{url('public/frontend/js/owl.carousel.min.js')}}"></script>

<script src="{{url('public/frontend/js/toastr.min.js')}}"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
    };
</script>
<script src="{{url('public/frontend/js/updatePass.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.4.0/js/intlTelInput.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{url('public/frontend/js/main.js?v='.time())}}"></script>

<script>
    /*G-Captcha Start*/
    var CaptchaCallback = function() {
            grecaptcha.render('gcaptcha001', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
            grecaptcha.render('gcaptcha002', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
            grecaptcha.render('gcaptcha003', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
            grecaptcha.render('gcaptcha004', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
            // grecaptcha.render('gcaptcha005', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
            // grecaptcha.render('gcaptcha006', {'sitekey' : "{{Config::get('constants.G_CAPTCHA_SITEKEY')}}",'callback' : verifyCallback,});
    };
    function gcaptcha001() {
		   localStorage.setItem("popup", "filled");

        var response = grecaptcha.getResponse(0);
        if (response.length == 0) {
            $('.g-recaptcha001-error').html('<span style="color:red;">This field is required.</span>');
            return false;
        }
        $('.g-recaptcha001-error').html('');
        $('button.disableFlag').prop('disabled', true); 
        $('button.disableFlag').text('Loading...');
        return true;        
    }

    function gcaptcha002() {
        var response = grecaptcha.getResponse(1);
        if (response.length == 0) {
            $('.g-recaptcha002-error').html('<span style="color:red;">This field is required.</span>');
            return false;
        }
        $('.g-recaptcha002-error').html('');
        $('button.disableFlag').prop('disabled', true); 
        $('button.disableFlag').text('Loading...');
        return true;        
    }

    function gcaptcha003() {
        var response = grecaptcha.getResponse(2);
        if (response.length == 0) {
            $('.g-recaptcha003-error').html('<span style="color:red;">This field is required.</span>');
            return false;
        }
        $('.g-recaptcha003-error').html('');
        $('button.disableFlag').prop('disabled', true); 
        $('button.disableFlag').text('Loading...');
        return true;        
    }

    function gcaptcha004() {
        var response = grecaptcha.getResponse(3);
        if (response.length == 0) {
            $('.g-recaptcha004-error').html('<span style="color:red;">This field is required.</span>');
            return false;
        }
        $('.g-recaptcha004-error').html('');
        $('button.disableFlag').prop('disabled', true); 
        $('button.disableFlag').text('Loading...');
        return true;        
    }

    function verifyCallback(response) {
        $('.g-recaptcha001-error').html('');
        $('.g-recaptcha002-error').html('');
        $('.g-recaptcha003-error').html('');
        $('.g-recaptcha004-error').html('');
        $('.g-recaptcha005-error').html('');
        $('.g-recaptcha006-error').html('');
    }
    /*G-Captcha End*/
    
    function removeClick(index, id) {
        var compareList = JSON.parse(localStorage.getItem("compareList") || "[]");

        for (var i = compareList.length - 1; i >= 0; i--) {
            var newStorage = compareList.filter(function(r,key) { return key != index });
            localStorage.setItem("compareList", JSON.stringify(newStorage));
        }

        $('#venueBlock'+id).css('display', 'none');
        $('#venueList'+id).css('display', 'none');

        if (compareList.length == 1) {
            var htmlList = '<div class="wrapper modal-title"> <p> Add venues or vendors to compare... </p> </div>'
            $('.compare-table').html(htmlList);
        }
    }

    $(function() {
        var success = {!! (Session::get('success') !== null)?"'".Session::get('success')."'":"''" !!};
        var error = {!! (Session::get('error') !== null)?"'".Session::get('error')."'":"''" !!};

        if(success != '') {
            toastr.success(success);
        }

        if(error != '') {
            toastr.error(error);
        }
    });
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    $('a.viewMoreFromBookingBtn').click(function () {
        $('#viewMoreFromBooking small.eventname').text($(this).data('eventname'));
        $('#viewMoreFromBooking small.eventtime').text($(this).data('eventtime'));
        $('#viewMoreFromBooking small.eventdate').text($(this).data('eventdate'));
        $('#viewMoreFromBooking small.eventtype').text($(this).data('eventtype'));
    });
</script>

<script>
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();

  // Vendor Tabs Continue
  var vendorContinueTab = '{{(isset(request()->continue))?request()->continue:""}}';
  // alert(vendorContinueTab);
  $('#'+vendorContinueTab).click();
});

var searchVenueLocality = function() {
    $.ajax({
        type: "GET",
        data: { "_token": "{{ csrf_token() }}"},
        url: "{{url('cities-return')}}",
        cache: false,
        success: function(msg){
            var data = JSON.parse(msg);
            // console.log(data);
            $('#searchVenueLocality').autocomplete({
              dataSource: data,
              defaultValue: {!! isset($current_city)?$current_city->id:'""' !!},
              textProperty:'name',
              valueProperty:'id',
            });
        } 
    })
};

var searchVenue = function () {
        $.ajax({
            type: "GET",
            data: { "_token": "{{ csrf_token() }}"},
            url: "{{url('venues-return')}}",
            cache: false,
            success: function(msg){
                var data = JSON.parse(msg);
                // console.log(data);
                $('#searchVenue').autocomplete({
                  dataSource: data,
                  defaultValue: {!! '""' !!},
                  textProperty:'name',
                  valueProperty:'id',
                });
            } 
        })
    searchVenueLocality();    
}

searchVenue();

// $('#searchVenueLocality').click(function() {
    

// });

// $('#searchVenue').click(function() {


// });

// $('#searchGlobal').on("change", function() {
//     $.ajax({
//         type: "GET",
//         data: { "_token": "{{ csrf_token() }}"},
//         url: "{{url('global-search')}}",
//         cache: false,
//         success: function(msg){
//             var data = JSON.parse(msg);
//             // console.log(data);
//             $('#searchGlobal').autocomplete({
//               dataSource: data,
//               defaultValue: {!! '""' !!},
//               textProperty:'name',
//               valueProperty:'id',
//             });
//         } 
//     })

// });

$(document).on("click", ".globalSearch", function(e) {
    // alert($(this).data('type'));
    var val = $(this).text();
    var type = $(this).data('type');

    $('#searchGlobal').val(val);
    $('#searchGlobalType').val(type);
    $("#search-suggestions").hide().html("");
});

$(document).on("click", "#globalSearchSubmit", function(e) {
    var value = $('#searchGlobal').val();
    var type = $('#searchGlobalType').val();
    var locality = $('#searchVenueLocality').val();
    e.preventDefault();
    if (type == 'venue') {
        window.location = '{{url("venues/filters")}}'+'?searchTerm='+value;
    }
    else {
        if (type == 'vendor_category') {
            window.location = '{{url("vendors/all")}}'+'?vendorCategory='+value+'&vendorLocality='+locality;
        }
        else {
            window.location = '{{url("vendors/all")}}'+'?vendorName='+value;
        }
    }
})
		
$(document).ready(function() {
    const vendorLocality = {!! isset($vendorLocality)?$vendorLocality:'[]' !!};
    const vendorCategories = {!! isset($vendorCategories)?$vendorCategories:'[]' !!};

    $('#searchSubmit').on('click', function(e) {
        const searchTerm = $('#searchVenue').val();
        const searchCity = $('#searchVenueLocality').val();
        e.preventDefault();
        if (searchCity != '') {
            $.ajax({
                url: '{{url("venues/searchCity")}}',
                method: "get",
                data: {city:searchCity},
                success: function (data) {
                    if (data == '0') {
                        window.location = '{{url("venues/filters")}}'+'?searchTerm='+searchCity;
                    }
                    else {
                        if (searchTerm != '') {
                            window.location = '{{url("venues")}}'+'/'+data.countryName.toLowerCase()+'/'+data.cityName.toLowerCase()+'?searchTerm='+searchTerm;
                        }
                        else {
                            window.location = '{{url("venues")}}'+'/'+data.countryName.toLowerCase()+'/'+data.cityName.toLowerCase();
                        }
                    }
                }
            });
        }
        if(searchTerm != '') {
            window.location = '{{url("venues/filters")}}'+'?searchTerm='+searchTerm;
        }
    })

    $('#searchVendorLocality').autocomplete({
      dataSource: vendorLocality,
      defaultValue: {!! '""' !!},
      textProperty:'name',
      valueProperty:'id',
      onClick: function(e) {
        console.log(e);
      }
    });

    $('#searchVendorCategory').autocomplete({
      dataSource: vendorCategories,
      defaultValue: {!! '""' !!},
      textProperty:'service',
      valueProperty:'id',
      onClick: function(e) {
          console.log(e);
      }  
    });

    // $('.autocomplete input').addClass('location');

    $('.compareClick').on('click', function(e) {

        // var values = [];
        // var contain = "COMP";
        var compareList = JSON.parse(localStorage.getItem("compareList") || "[]");

        if (compareList != "[]" && compareList.length == 4) {
            // alert('Comparison of more than four venues is not allowed.');
            console.log(compareList.length);
            toastr.error('Comparison of more than four venues is not allowed.');            
        }
        else {
            var id          = $(this).data('id');
            var venue       = $(this).data('venue');
            var price       = $(this).data('price');
            var guestsMin   = $(this).data('guestsmin');
            var guestsMax   = $(this).data('guestsmax');
            var rooms       = $(this).data('rooms');
            var covid       = $(this).data('covid');
            var banquet     = $(this).data('banquet');
            var bar         = $(this).data('bar');
            var dj          = $(this).data('dj');
            var resorts     = $(this).data('resorts');
            var cpolicy     = $(this).data('cpolicy');
            var oAminities  = $(this).data('oAminities');
            var img         = $(this).data('img');
            var idFlag;

            $.each(compareList, function(index, obj) {
                if(obj.id == id){
                    idFlag = true;
                }
                else {
                    idFlag = false;
                }
            })

            if (idFlag) {
                // alert('Already added to the list.');
                toastr.warning('Already added to the list.');   
            }
            else {
                var data = {
                    id, venue, price, guestsMin, guestsMax, rooms, covid, banquet, bar, dj, resorts, cpolicy, oAminities, img
                };
                
                compareList.push(data);
                localStorage.setItem("compareList", JSON.stringify(compareList));
                toastr.success('venue added to compare list...');
            }
        }
        callback();
    });

    function callback() {
        var compareList = JSON.parse(localStorage.getItem("compareList") || "[]");
        if(compareList != "[]" && compareList.length > 0) {
            var venueList = ''; 
            var htmlList = '';
            for (var i = compareList.length - 1; i >= 0; i--) {

                htmlList += '<div class="col-sm-4 col-6 compare-block" id="venueBlock'+compareList[i].id+'">';
                htmlList += '<div class="compare-table-image">'+
                                '<img class="img-fluid" src="{{url("/")}}/'+compareList[i].img+'" alt="..." />'+
                            '</div>'+
                            '<h3 class="compare-vendor-title"><a href="#">'+compareList[i].venue.substring(0, 14)+'...</a> <span class="text-left" id="venueRemove'+compareList[i].id+'"><button class="btn-danger btn-sm removeClick" data-index="'+i+'" data-id="'+compareList[i].id+'" title="Remove from compare list..." onclick="return removeClick('+i+', '+compareList[i].id+')">x</button></span> </h3>'+
                            '<div class="compare-wrap">'+
                                '<p class="subhead">Price Per Plate</p>'+
                                '<p>Rs. '+((compareList[i].price != '') ? compareList[i].price : '-')+'</p>'+
                                '<p class="subhead">No. of Guests</p>'+
                                '<p>'+((compareList[i].guestsMin != '') ? 'upto '+compareList[i].guestsMin : '-' )+'</p>'+
                                '<p class="subhead">Number of Rooms</p>'+
                                '<p>'+((compareList[i].rooms != '') ? 'upto '+compareList[i].rooms : '-' )+'</p>'+
                                '<p class="subhead">COVID Ready?</p>'+
                                '<p>'+((compareList[i].covid != 'Yes') ? '<i class="fa fa-check-square-o"></i> Yes' : '<i class="fa fa-close"></i> No' )+'</p>'+
                                '<p class="subhead">Banquet</p>'+
                                '<p>'+((compareList[i].banquet == 'Yes') ? '<i class="fa fa-check-square-o"></i> Yes' : '<i class="fa fa-close"></i> No' )+'</p>'+
                                '<p class="subhead">Bar</p>'+
                                '<p>'+((compareList[i].bar == 1) ? '<i class="fa fa-check-square-o"></i> Yes' : '<i class="fa fa-close"></i> No' )+'</p>'+
                                '<p class="subhead">DJ & Concert</p>'+
                                '<p><i class="fa fa-check-square-o"></i> Yes</p>'+
                                '<p class="subhead">Lawn</p>'+
                                '<p>'+((compareList[i].resorts != 'Yes') ? '<i class="fa fa-check-square-o"></i> Yes' : '<i class="fa fa-close"></i> No' )+'</p>'+
                                '<p class="subhead">Resort</p>'+
                                '<p>'+((compareList[i].resorts != 'Yes') ? '<i class="fa fa-check-square-o"></i> Yes' : '<i class="fa fa-close"></i> No' )+'</p>'+
                                '<p class="subhead">Cancellation Policy</p>'+
                                '<p>'+((compareList[i].cpolicy != '') ? '<i class="fa fa-check-square-o"></i>'+compareList[i].cpolicy : '<i class="fa fa-close"></i> Non-Refundable' )+'</p>'+
                                '<p class="subhead">Other Amenities</p>'+
                                '<p>Guest accommodation, pool, bridal dressing room , parking area, in-house restaurant</p>'+
                            '</div>'+
                            '<a href="#" class="btn btn-primary OfferModal" data-toggle="modal" data-target="#bestOfferModal" data-id="'+compareList[i].id+'" data-venue="'+compareList[i].venue+'">Book Now</a>';
                htmlList += '</div>';
                venueList += '<span id="venueList'+compareList[i].id+'">'+compareList[i].venue+((i==0)?'':' | ')+'</span>';
            }

            $('.compare-table').html(htmlList);
            $('.venueList').html(venueList);
        }
        else {
            var htmlList = '<div class="wrapper modal-title"> <p> Add venues or vendors to compare... </p> </div>'
            $('.compare-table').html(htmlList);
        }
    }
    callback();

    $('.OfferModal').on('click', function () {
        var id      = $(this).data('id');
        var venue   = $(this).data('venue');
        var source   = $(this).data('source');
         console.log(source);
        $('#venueName, .venueName').val(venue);
        $('#venueId, .venueId').val(id);
        $('#source, .source').val(source);
        //  alert(source);
    });

    $('.OfferModalVendor').on('click', function () {
        var id      = $(this).data('id');
        var vendor   = $(this).data('vendor');
        var source   = $(this).data('source');
        var vendorEmail = $(this).data('vendoremail');
         console.log(source);
        $('#vendorName').val(vendor);
        $('#vendorId').val(id);
        $('#source').val(source);
        $('#vendorEmail').val(vendorEmail);
        //  alert(source);
    });
    
    $('.bottomTileClick').on('click', function () {
        var lead_type = $(this).data('lead_type');
        $('input[name="lead_type"]').val(lead_type);     
    });

    $('.shorlistedMailShare').on('click', function () {
        var userid = $(this).data('userid');
        var username = $(this).data('username');
        $('input#userid').val(userid);
        $('input#username').val(username);
    });

    // $('.offerPopup').on('submit', function(e) {
    //     e.preventDefault();
    //     var data = $(this).serialize();
    //     console.log(data);
    // });


    $('form.register').submit( function(e) {
        e.preventDefault();
        var mobile_no = $('#registerPhone').val();
        var otp = $('#registerOtp').val();
        var userType  = $('#userType').val();
        if (mobile_no == '') {
            toastr.warning('Enter mobile number...');
            return false;
        }
        if (otp == '') {
            toastr.warning('Enter OTP number...');
            return false;
        }
        if (!$('[name="terms&conditions"]').is(':checked')) {
            toastr.warning('Accept the Terms & Conditions...');
            return false;
        }
        $.ajax({
            type: "POST",
            data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no, otp:otp, userType:userType},
            url: "{{url('verify-otp')}}",
            cache: false,
            success: function(msg){
                var data = JSON.parse(msg);
                if (data.status == 'success') {
                    $('#registerBtn').prop('disabled', true);
                    $('#registerBtn').html('Loading...');
                    $('.register').unbind('submit').submit();
                }
                if (data.status == 'error') {
                    toastr.error(data.msg);
                }
            }    
        });
    });    

    $('#regOtpClick').click(function(event) {
        var mobile_no = $('#registerPhone').val();
        var userType  = $('#userType').val();
        if(mobile_no == '') {
            toastr.warning('Enter mobile number...');
        }
        else {
            $.ajax({
                type: "POST",
                data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no, userType:userType},
                url: "{{url('check-user-mobile')}}",
                cache: false,
                success: function(msg){
                    var data = JSON.parse(msg);
                    if (data.status == 'error') {
                        toastr[data.status](data.msg);
                        $('input.otp').css('display', 'none');  
                        return false;
                    }
                    if (data.status == 'success') {   
                        toastr[data.status](data.msg);
                        $.ajax({
                            type: "POST",
                            data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no},
                            url: "{{url('generate-otp')}}",
                            success: function(msg){
                                console.log(msg);
                                var data = JSON.parse(msg);
                                toastr[data.status](data.msg);
                            }
                        });
                    }
                }
            })
        }
    })

    // $('#loginBtn').click(function(e) {
    //     var mobile_no = $('#loginPhone').val();
    //     var otp = $('#loginOtp').val();
    //     if (mobile_no == '') {
    //         toastr.warning('Enter mobile number...');
    //         return false;
    //     }
    //     if (otp == '') {
    //         toastr.warning('Enter OTP number...');
    //         return false;
    //     }
    //     $.ajax({
    //         type: "POST",
    //         data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no, otp:otp},
    //         url: "{{url('verify-otp')}}",
    //         cache: false,
    //         success: function(msg){
    //             var data = JSON.parse(msg);
    //             if (data.status == 'success') {
    //                 $('.login').submit();
    //             }
    //             if (data.status == 'error') {
    //                 toastr.error(data.msg);
    //                 $('#loginBtn').prop('disabled', false);
    //                 $('#loginBtn').html('Login');
    //             }
    //         }    
    //     });

    //     $('#loginBtn').prop('disabled', true);
    //     $('#loginBtn').html('Loading...');
    // });

    // $('#loginOtpClick').click(function(event) {
    //     var mobile_no = $('#loginPhone').val();
    //     if(mobile_no == '') {
    //         toastr.warning('Enter mobile number...');
    //     }
    //     else {
    //         $.ajax({
    //             type: "POST",
    //             data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no},
    //             url: "{{url('check-user-mobile')}}",
    //             cache: false,
    //             success: function(msg){
    //                 var data = JSON.parse(msg);
    //                 if (data.status == 'error') {
    //                     $.ajax({
    //                         type: "POST",
    //                         data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no},
    //                         url: "{{url('generate-otp')}}",
    //                         success: function(msg){
    //                             var data = JSON.parse(msg);
    //                             if (data.status == 'success') {
    //                                 toastr[data.status](data.msg);
    //                             }
    //                             else {
    //                                 $('input.otp').css('display', 'none');
    //                                 toastr[data.status](data);
    //                             }
    //                         }
    //                     });  
    //                 }
    //                 if (data.status == 'success') {   
    //                     $('input.otp').css('display', 'none');
    //                     toastr['error']('Mobile number is not registered...');
    //                     return false;
    //                 }
    //             }
    //         })
    //     }
    // })

    $('.forgotPasswordForm').on('submit', function(event) {
        event.preventDefault();

        $('#forgotPasswordBtn').prop('disabled', true);
        $('#forgotPasswordBtn').html('Loading...');

        var mobile_no = $('#forgotPhone').val();
        var from =  $('input#from').val();
        if(mobile_no == '') {
            toastr.warning('Enter mobile number...');
        }
        else {
            $.ajax({
                type: "POST",
                data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no, from:from},
                url: "{{url('check-user-mobile')}}",
                cache: false,
                success: function(msg){
                    var data = JSON.parse(msg);
                    if (data.status == 'error') {
                        $.ajax({
                            type: "POST",
                            data: { "_token": "{{ csrf_token() }}", mobile_no:mobile_no, from:from},
                            url: "{{url('forgot-password')}}",
                            success: function(msg){
                                var data = JSON.parse(msg);
                                if (data.status == 'success') {
                                    toastr[data.status](data.msg);
                                    $('#forgotPasswordBtn').prop('disabled', true);
                                    $('#forgotPasswordBtn').html('Loading...');
                                    $('#forgotPasswordModal').modal('toggle');
                                    $('div.fade').removeClass('modal-backdrop');
                                }
                                else {
                                    // toastr[data.status]('Entered mobile number does not exist...');
                                    toastr[data.status](data.msg);
                                    $('#forgotPasswordBtn').prop('disabled', false);
                                    $('#forgotPasswordBtn').html('<i class="fa fa-paper-plane"></i>Send My Password');
                                }
                            }
                        });  
                    }
                    if (data.status == 'success') {   
                        toastr['error']('Mobile number is not registered...');
                        $('#forgotPasswordBtn').prop('disabled', false);
                        $('#forgotPasswordBtn').html('<i class="fa fa-paper-plane"></i>Send My Password');
                        $('#forgotPasswordModal').modal('toggle');
                        $('div.fade').removeClass('modal-backdrop');
                    }
                }
            })
        }
    })

    // $('#searchTerm').easyAutocomplete(options);
});    
</script>
<script>
    $("#contact-btn").click(function () {
    //alert("in");
    
    if($('#semail').val()=="")
    {
        $('.error').html('Contact Number Field is Required');
        return false;
    }


    if($('#semail').val().length !=10)
    {
        $('.error').html('Invalid Contact Number');
        return false;
    }
    //return false;
                    
                
    var not = $('#semail').val();
    var dataString = 'not=' +not;

    //return false;
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
        data: { "_token": "{{ csrf_token() }}",not:not},
        url: "{{url('drop_number')}}",
        cache: false,
        success: function(msg){
            $('.error').html("Request Sent Successfully");
            $('#semail').val('');
        }
    });
    return false;
});                
</script>

<script>
    $('.shortlistClick').on('click', function(e){
        var id = $(this).data('id');
        var userid  = $(this).data('userid');
        var shortlistof = $(this).data('shortlistof');

        if(userid == ''){
            // alert('Please login first...');
            toastr.warning('Please login first...');
        }
        else{
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                data: { "_token": "{{ csrf_token() }}", id:id, userid:userid, shortlistof:shortlistof},
                url: "{{url('add-shortlist')}}",
                cache: false,
                success: function(msg){
                    if(msg == 'already') {
                        // alert('venue already shortlisted...');
                        toastr.warning(shortlistof+' already shortlisted...');
                    }
                    if(msg == 'true') {
                        // alert('venue shortlisted successfully...');
                        toastr.success(shortlistof+' shortlisted successfully...');
                    }
                    if(msg == 'false') {
                        // alert('something went wrong...');
                        toastr.error('something went wrong...');
                    }
                }    
            });
        }
    });

    $('.removeShortlist').on('click', function(e){
        var id = $(this).data('id');
        var userid  = $(this).data('userid');
        var shortlistof = $(this).data('shortlistof');

        if(userid == ''){
            alert('Please login first...');
            location.reload();
        }
        else{
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                data: { "_token": "{{ csrf_token() }}", id:id, userid:userid, shortlistof:shortlistof},
                url: "{{url('remove-shortlist')}}",
                cache: false,
                success: function(msg){
                    if(msg == 'true') {
                        alert(shortlistof+' removed from shortlist...');
                    }
                    if(msg == 'false') {
                        // alert('something went wrong...');
                        toastr.error('something went wrong...');
                    }
                }    
            });
            location.reload();
        }
    });
</script>
<script>
    $(document).on("input", "#search-box", function (e) {
            var t = window.SITEURL + "search-suggestions";
            window.xhr && window.xhr.abort();
            var n = e.target.value;
            n ? window.xhr = $.get(t, {
                keyword: n
            }, function (e) {
                $("#search-suggestions").show().html(e.map(function (e) {
                    return "<li>" + e + "</li>"
                }).join("\n"))
            }) : $("#search-suggestions").hide().html("")
        }), $("#search-box").click(function (e) {
            return $("#search-suggestions").show()
        }), $("#search-box").on("keydown", function (e) {
            if (/Arrow(Up|Down)/.test(e.key)) {
                var t, n = null;
                $("#search-suggestions li.active").length ? 0 === (n = $("#search-suggestions li.active")[e.key.includes("Down") ? "next" : "prev"]().addClass("active").siblings().removeClass("active").end()).length && (n = $("#search-suggestions li:" + (e.key.includes("Down") ? "first" : "last")).addClass("active").siblings().removeClass("active").end()) : n = $("#search-suggestions li:" + (e.key.includes("Down") ? "first" : "last")).addClass("active").siblings().removeClass("active").end(), n[0].scrollIntoView(), (t = n.text()) && $(e.target).val(t)
            } else "Enter" == e.key && (window.location.href = $("#search-suggestions li.active a").attr("href"))
        }), $("body").on("click", function (e) {
            $(e.target).is("#search-box, #search-suggestions *") || $("#search-suggestions").hide()
        }),$(document).on("input", "#vendor-search-box", function (e) {
            var t = window.SITEURL + "vendor-search-suggestions";
            window.xhr && window.xhr.abort();
            var n = e.target.value;
            n ? window.xhr = $.get(t, {
                keyword: n
            }, function (e) {
                $("#vendor-search-suggestions").show().html(e.map(function (e) {
                    return "<li>" + e + "</li>"
                }).join("\n"))
            }) : $("#vendor-search-suggestions").hide().html("")
        }),$(document).on("input", "#global-search-box", function (e) {
            var t = window.SITEURL + "global-search";
            window.xhr && window.xhr.abort();
            var n = e.target.value;
            n ? window.xhr = $.get(t, {
                keyword: n
            }, function (e) {
                $("#search-suggestions").show().html(e.map(function (e) {
                    return "<li>" + e + "</li>"
                }).join("\n"))
            }) : $("#search-suggestions").hide().html("")
        }),$(document).on("input", "#searchGlobal", function (e) {
            var t = window.SITEURL + "global-search";
            window.xhr && window.xhr.abort();
            var n = e.target.value;
            n ? window.xhr = $.get(t, {
                keyword: n
            }, function (e) {
                $("#search-suggestions").show().html(e.map(function (e) {
                    return "<li>" + e + "</li>"
                }).join("\n"))
            }) : $("#search-suggestions").hide().html("")
        })
</script>
<script>
    var GA_FLAG = parseInt("1");
    navigator.who=function(){var e,t=navigator.userAgent,a=t.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i)||[];return/trident/i.test(a[1])?(e=/\brv[ :]+(\d+)/g.exec(t)||[],"IE "+(e[1]||"")):"Chrome"===a[1]&&(e=t.match(/\b(OPR|Edge)\/(\d+)/),null!=e)?e.slice(1).join(" ").replace("OPR","Opera"):(a=a[2]?[a[1],a[2]]:[navigator.appName,navigator.appVersion,"-?"],null!=(e=t.match(/version\/(\d+)/i))&&a.splice(1,1,e[1]),a.join(" "))}(),navigator.isMobileTablet=function(){var e,t=!1;return e=navigator.userAgent||navigator.vendor||window.opera,(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(e)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0,4)))&&(t=!0),t}(),navigator.isMobile=function(){var e,t=!1;return e=navigator.userAgent||navigator.vendor||window.opera,(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(e)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0,4)))&&(t=!0),t}();

    function socialSharingButton(e, ele, ss_type) {
        e.preventDefault();
        var url = "https://" + location.hostname + location.pathname;
        // var title = document.title;
        var title = document.querySelector('h1.title').innerText;
        var desc = jQuery("meta[name=description]").attr("content");
        var shareUrl = location.href;
        var isMobileTablet = navigator.isMobileTablet;
        var query = "";
        var data_title  =   JSON.parse('{}');

        if (ss_type === "WhatsApp") {
            if(data_title && data_title.whatsapp){
                url =   url + '\n' + data_title.whatsapp;
            }
            query = jQuery.param({
                text: url
            });
            shareUrl = "https://"+(isMobileTablet?'api':'web')+".whatsapp.com/send?" + query;
        } else if (ss_type === "Facebook") {
            query = jQuery.param({
                u: url,
                title: title
            });
            shareUrl = "https://www.facebook.com/sharer.php?" + query;
        } else if (ss_type === "Mobile") {
            if (navigator.share === undefined) {
                query = jQuery.param({
                    u: url,
                    title: title
                });
                shareUrl = "https://www.facebook.com/sharer.php?" + query;
                ss_type = "Facebook";
            } else {
                if (GA_FLAG)
                    // ga(
                    //     "send",
                    //     "event",
                    //     ss_type + "-Share",
                    //     "social share click",
                    //     "page: " + C_URL
                    // );

                navigator
                    .share({
                        title: title,
                        url: url,
                        text: title
                    })
                    .then(function () {})
                    .catch(function (error) {});

                return false;
            }
        } else if (ss_type === "Twitter") {
            query = jQuery.param({
                text: title,
                url: url,
                via: "BYJUS"
            });
            shareUrl = "https://twitter.com/intent/tweet/?" + query;
        } else if (ss_type === "Linkedin") {
            query = jQuery.param({
                mini: true,
                url: url,
                title: title
            });
            shareUrl = "https://www.linkedin.com/cws/share?" + query;
        }

        if (GA_FLAG)
            // ga(
            //     "send",
            //     "event",
            //     ss_type + "-Share",
            //     "social share click",
            //     "page: " + C_URL
            // );

        var top = (screen.availHeight - 500) / 2;
        var left = (screen.availWidth - 500) / 2;
        var popup = window.open(
            shareUrl,
            "social sharing",
            "width=550,height=420,left=" +
            left +
            ",top=" +
            top +
            ",location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1"
        );
        return false;
    }
    jQuery(document).ready(function () {
        if (navigator.isMobileTablet) {
            // social sharing btns
            var socialBtns = jQuery("#social-sharing-btns");
            var h1Ele = jQuery(".h1-1r-title");
            if (socialBtns && socialBtns.length && h1Ele && h1Ele.length) {
                socialBtns.css(
                    "top",
                    h1Ele.offset().top + h1Ele.outerHeight() + 10 + "px"
                );
            }
        }
        // console.log(navigator.share);
        // console.log(navigator.who);
        //navigator share for mobile
        if (navigator.share !== undefined) {
            jQuery("#social-sharing-btns .button-facebook").addClass("hidden").addClass('d-none');
            jQuery("#social-sharing-btns .button-mobile").removeClass("hidden").removeClass('d-none'); // add for Bootstrap 4
        }

        // Vendor Image Deletion
        if ($("[id^='removImage___']").length > 0) {

            $("[id^='removImage___']").click(function () {
            var id = $(this).attr('id').split('___')[1];
            var oldpath = $(this).attr('id').split('___')[2];

            // alert(oldpath);
            // return false;

            $.ajax({
                url: SITEURL + '/remove-imgs',
                headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    path: oldpath
                },
                success: function (msg)
                {
                    if (msg == 1) {
                        $('#imgSrc_' + id).hide("slow");
                    } else {
                        alert('Something went wrong');
                    }
                }
            });
        });

        }
        
    });
</script>
</body>

</html>